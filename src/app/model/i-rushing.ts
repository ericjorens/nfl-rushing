export interface IRushing {
  Player: string;
  Team: string;
  Pos:string;
  Att:number;
  "Att/G":number;
  Yds:number;
  Avg:number;
  "Yds/G":number;
  TD:number;
  Lng:number;
  "1st":number;
  "1st%":number;
  "20+":number;
  "40+":number;
  FUM:number;
}
