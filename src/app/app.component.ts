import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, withLatestFrom, startWith } from 'rxjs/operators';
import data from '../assets/rushing.json';
import { SortDirective } from './directive/sort.directive';
import { IRushing } from './model/i-rushing';
import { CsvDataService } from './service/csv-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'nfl-rushing';
  searching = '';
  formGroup: FormGroup;
  rushing: IRushing[] = data as IRushing[];
  rushing$: Observable<IRushing[]>;
  filteredRushing$: Observable<IRushing[]>;

  constructor(private formBuilder: FormBuilder) {
    this.formGroup = formBuilder.group(
      { filter: [''] },
      { updateOn: 'change' }
    );
    let filter = this.formGroup.get('filter');
    this.rushing$ = this.getRushing();
    this.filteredRushing$ = this.getRushing();
    if (!!filter) {
      this.searching = filter.value;
      this.filteredRushing$ = filter.valueChanges.pipe(
        startWith(''),
        withLatestFrom(this.rushing$),
        map(([val, r]) =>
          !val
            ? r
            : r.filter((x) => {
                return (
                  x.Player.includes(val) ||
                  x.Team.includes(val) ||
                  x.Pos.includes(val) ||
                  x.Att == val ||
                  x['Att/G'] == val ||
                  x.Yds == val ||
                  x.Avg == val ||
                  x['Yds/G'] == val ||
                  x.TD == val ||
                  x.Lng == val ||
                  x['1st'] == val ||
                  x['1st%'] == val ||
                  x['20+'] == val ||
                  x['40+'] == val ||
                  x['FUM'] == val
                );
              })
        )
      );
    }
    this.filteredRushing$.subscribe((r: IRushing[]) => {
      this.rushing = Object.assign(r);
    });
  }

  download() {
    let filter = this.formGroup.get('filter');
    let title = this.title;
    if (!!filter) {
      title += '-' + filter.value;
    }
    CsvDataService.exportToCsv(title, this.rushing);
  }

  clearSort() {
    SortDirective.clearArrows();
  }

  private getRushing(): Observable<any[]> {
    return of(data);
  }
}
