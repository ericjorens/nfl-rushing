import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
} from '@angular/core';
import { Observable } from 'rxjs';
import { IRushing } from '../model/i-rushing';
import { Sort } from '../util/sort';

@Directive({
  selector: '[appSort]',
})
export class SortDirective {
  @Input() appSort: Observable<IRushing[]> | undefined;

  constructor(private renderer: Renderer2, private targetElem: ElementRef) {}

  @HostListener('click')
  sortData() {
    this.appSort?.subscribe((rushing: IRushing[]) => {
      const sort = new Sort();
      const elem = this.targetElem.nativeElement;
      const order = elem.getAttribute('data-order');
      const type = elem.getAttribute('data-type');
      const property = elem.getAttribute('data-name');
      rushing.sort(sort.startSort(property, order, type));
      //redraw the header arrows
      SortDirective.clearArrows();
      if (order === 'desc') {
        elem.setAttribute('data-order', 'asc');
        elem.innerHTML = property + '&nbsp;&uarr;';
      } else {
        elem.setAttribute('data-order', 'desc');
        elem.innerHTML = property + '&nbsp;&darr;';
      }
    });
  }

  //clear the header arrows
  static clearArrows() {
    const heads = document.getElementsByClassName('s-header');
    for (let i = 0; i < heads.length; i++) {
      let property = heads[i].getAttribute('data-name');
      if (!!property) {
        heads[i].innerHTML = property;
      }
    }
  }
}
