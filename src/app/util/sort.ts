export class Sort {
  private so = 1;
  private collator = new Intl.Collator(undefined, {
    numeric: true,
    sensitivity: 'base',
  });

  constructor() {}

  public startSort(prop: string, o: string, t = '') {
    if (o === 'desc') {
      this.so = -1;
    }
    return (a: any, b: any) => {
      if (t === 'date') {
        return this.sortData(new Date(a[prop]), new Date(b[prop]));
      } else {
        return this.collator.compare(a[prop], b[prop]) * this.so;
      }
    };
  }

  private sortData(a: any, b: any): number {
    if (a < b) {
      return -1 * this.so;
    } else if (a > b) {
      return 1 * this.so;
    } else {
      return 0 * this.so;
    }
  }
}
