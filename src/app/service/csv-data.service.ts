import { IRushing } from '../model/i-rushing';

export class CsvDataService {
  static exportToCsv(filename: string, rushings: IRushing[]) {
    //fail fast
    if (!rushings || !rushings.length) {
      return;
    }
    const separator = ',';
    const keys = [
      'Player',
      'Team',
      'Pos',
      'Att',
      'Att/G',
      'Yds',
      'Avg',
      'Yds/G',
      'TD',
      'Lng',
      '1st',
      '1st%',
      '20+',
      '40+',
      'FUM',
    ];

    let content = keys.join(separator) + '\n';

    rushings.forEach((r: IRushing) => {
      content +=
        r.Player +
        separator +
        r.Team +
        separator +
        r.Pos +
        separator +
        r.Att +
        separator +
        r['Att/G'] +
        separator +
        r.Yds +
        separator +
        r.Avg +
        separator +
        r['Yds/G'] +
        separator +
        r.TD +
        separator +
        r.Lng +
        separator +
        r['1st'] +
        separator +
        r['1st%'] +
        separator +
        r['20+'] +
        separator +
        r['40+'] +
        separator +
        r['FUM'] +
        '\n';
    });

    const blob = new Blob([content], { type: 'text/csv;charset=utf-8;' });
    const link = document.createElement('a');

    if (link.download !== undefined) {
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', filename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}
